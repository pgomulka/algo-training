package hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
public class MakingAnagrams {
    public static int numberNeeded(String first, String second) {
        Map<Character,Integer> m1 = new HashMap<>();
        Map<Character,Integer> m2 = new HashMap<>();

        for(Character c: first.toCharArray()){
            int count = m1.getOrDefault(c,0);
            m1.put(c,count+1);
        }
        for(Character c: second.toCharArray()){
            int count = m2.getOrDefault(c,0);
            m2.put(c,count+1);
        }

        int sum = 0;
        for(Character c : m1.keySet()){
            int d = m2.getOrDefault(c,0);
            sum += Math.abs(d-m1.get(c));
        }

        for(Character c : m2.keySet()){
            if(!m1.keySet().contains(c))
                sum+=m2.get(c);
        }

        return sum;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String a = in.next();
        String b = in.next();
        System.out.println(numberNeeded(a, b));
    }
}