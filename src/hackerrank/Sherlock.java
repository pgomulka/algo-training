package hackerrank;

import java.util.*;
import java.util.stream.Collectors;

public class Sherlock {
    static String isValid(String s){
        Map<Character,Integer> histo = new HashMap<>();
        for(Character c : s.toCharArray()) {
            histo.merge(c, 1, (p,n)->p+n);
        }

        int removed = 0;
        int p = -1;
        int c = 0;
        List<Integer> sorted = histo.values().stream().sorted().collect(Collectors.toList());
        for(Integer v : sorted){
            if(p==-1) {
                p = v;

            }else if(p != v){
                if(c == 1 && p == 1){
                    removed ++;
                    p = v;
                }else if( Math.abs(p-v) == 1) {
                    removed ++;
                } else{
                    return "NO";
                }

            }
            c++;

        }

        return removed <= 1 ? "YES"  : "NO ";

    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        String result = isValid("aabbcd");
        System.out.println(result);
    }
}
