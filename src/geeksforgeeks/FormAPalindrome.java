package geeksforgeeks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

//http://www.practice.geeksforgeeks.org/problem-page.php?pid=557
// form a palindrome
public class FormAPalindrome {
    static Map<String, Integer> map = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        for (int i = 0; i < n; i++) {
            String s = reader.readLine();
            System.out.println(compute(s));
        }
    }

    private static boolean isPalindrom(String s) {
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

    private static int compute(String s) {
        int ret;
        if (map.containsKey(s)) return map.get(s);
        if (s.length() <= 1) ret = 0;
        else if (isPalindrom(s)) ret = 0;
        else {
            if (s.charAt(0) == s.charAt(s.length() - 1))
                ret = compute(s.substring(1, s.length() - 1));
            else {

                ret = Math.min(
                        compute(s.substring(0, s.length() - 1)) + 1,
                        compute(s.substring(1, s.length())) + 1
                );
            }

        }
        map.put(s, ret);
        return ret;
    }
}